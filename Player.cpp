#include "Player.h"

Player::Player(int id, string faction)
	:id(id),faction(faction){
	
	planets = new int[12];
	for(int i = 0; i < 12; i++){
		planets[i] = -1;
	}
}

void Player::setPlayerID(int i){
	id = i;
}
void Player::setFaction(string s){
	faction = s;
}
void Player::setPlanets(int i, int planetNum){
	planets[i] = planetNum;
}

int Player::getPlayerID(){
	return id;
}
string Player::getFaction(){
	return faction;
}

int Player::getPlanet(int i){
	return planets[i];
}
