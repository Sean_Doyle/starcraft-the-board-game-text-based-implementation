#include "Main.h"


bool factionAvailablity[6] = {true,true,true,true,true,true};
bool planetAvailabilty[12] = {true,true,true,true,true,true,true,true,true,true,true,true};

vector<Player> players;

int main(){
	
	gamestate = GAME_START;
	isRunning = true;
	srand(time(0));
	while(isRunning){
		switch(gamestate){
		case GAME_START:
			startMenu();
			break;
		case GAME_SETUP:
			selectPlayerAmount();
			selectPlayerFactions();
			assignPlanets();
			printPlayerDetails();
			galaxyInformation();
			galaxyCreation();			

			break;
		case GAME_RUN:
			printCurrentGalaxy();
			quit();
			break;
		case GAME_END:
			isRunning = false;
			break;
		}
	}
	return 1;
}

void startMenu(){
	cout << "-----------------------------" << endl 
		 << "STARCRAFT THE BOARD GAME!! \n" << endl
		 << "-----------------------------" << endl << endl
		 << "1) Start New Game" << endl
		 << "2) Quit" << endl << endl;
	
	int startMenuOption;
	do{
		cout << "Select Option: ";
		cin >> startMenuOption;
	}while(!validChoice(startMenuOption, 1, 2));

	if(startMenuOption == 1){
		system("cls");
		gamestate = GAME_SETUP;
	}else
		gamestate = GAME_END;
}

void quit(){
	cout << endl << "The galaxy has been successfully created" << endl;
	system("pause");
	gamestate = GAME_END;
}

void selectPlayerAmount(){
	do{
		cout << "Please Enter How Many Players Are Playing(2-6): ";
		cin >> playerCount;
	}while(!validChoice(playerCount, 2, 6));
	
}

void selectPlayerFactions(){
	int playerSelection;
	bool validOption;

	for(int i = 0; i < playerCount; i++){
		cout << endl;
		system("cls");
		printFactions();
		do{
			validOption = false;
			cout << endl << "Player " << i+1 << ", Please Select A Faction(1-6) :";
			cin >> playerSelection;

			if(validChoice(playerSelection,1,6) && testFactionAvailablity(playerSelection-1) == "Available"){
				factionAvailablity[playerSelection-1] = false;
				validOption = true;
			}
		}while(!validOption);
		
		Player p(i+1,faction[playerSelection]);
		players.push_back(p);
	}
}

void assignPlanets(){
	bool validPlanet = false;
	int x = 0, y = 0;

	cout << "Players will now be assigned 2 planets at random. \n";
	for(unsigned int i = 0; i < players.size(); i++){
		x = 0;
		while(x < 2){
			validPlanet = false;
			do{
				y = rand() %12;
				
				if(testPlanetAvailabilty(y)){
				
					planetAvailabilty[y] = false;
					players[i].setPlanets(x,y);
					validPlanet = true;
				}	
			}while(!validPlanet);
			x++;
		}
	}
}

void galaxyInformation(){
	for(int a = 0; a < 6; a++){
		for(int b = 0; b < 6; b++){
			galaxyMap[a][b] = " ";
		}
	}

	system("pause");
	system("cls");

	cout << "#$#  Instructions for Creating The Galaxy  #$#" << endl
		 << "----------------------------------------------" << endl
		 << " Each player will take turns to specify where" << endl
		 << " they will place their planets on the map. " << endl
		 << " Planets must be placed next to another planet " << endl
		 << " that is present on the map at that moment. " << endl
		 << " Only Player 1's first planet is an exception" << endl
		 << " players will place one planet per turn" << endl << endl
		 << "Player 1's planets will be represented by : X" << endl
		 << "Player 2's planets will be represented by : O" << endl;

	string restOfInstructions;
	if(playerCount == 6)
		restOfInstructions = "Player 3's planets will be represented by : & \nPlayer 4's planets will be represented by : $ \nPlayer 5's planets will be represented by : K \nPlayer 6's planets will be represented by : % \n";
	else if(playerCount == 5)
		restOfInstructions = "Player 3's planets will be represented by : & \nPlayer 4's planets will be represented by : $ \nPlayer 5's planets will be represented by : K \n";
	else if(playerCount == 4)
		restOfInstructions = "Player 3's planets will be represented by : & \nPlayer 4's planets will be represented by : $ \n";
	else if(playerCount == 3)
		restOfInstructions = "Player 3's planets will be represented by : & \n";

	cout << restOfInstructions << endl;
	system("pause");
	system("cls");
}

void galaxyCreation(){

	string location;
	string locX,locY;
	int xPos,yPos;

	printCurrentGalaxy();
	//First Planet - Starting point
	do{
		cout << "Player 1 specify any location to place your first planet(format: \"4\\3\"): ";
		cin >> location;
	}while(!validLocation(location));
	
	locX = location[0];
	locY = location[2];
	
	xPos =  stoi(locX,0,10);
	yPos =  stoi(locY,0,10);
	
	galaxyMap[xPos-1][yPos-1] = "X";

	//All remaining players
	int x = 0;
	while(x < 2){
		for(int i = 0; i < playerCount; i++){
			system("cls");
			printCurrentGalaxy();
			bool planetPlaced = false;
			if(x == 0 && i == 0){//as player 1 assigns the first planet freely with no constraints.

			}else{
				do{
					//Check location is within boundries.
					do{
						cout << "Player " << i+1 << " specify a location next to a currently placed planet(format: \"4\\3\"): ";
						cin >> location;
					}while(!validLocation(location));

					locX = location[0];
					locY = location[2];
	
					xPos =  stoi(locX,0,10) -1;
					yPos =  stoi(locY,0,10) -1;
					//Check location is beside a planet
					if(validPlacement(xPos,yPos)){
						planetPlaced = true;
						placePlanet(i,xPos,yPos);
					}
				}while(!planetPlaced);
			}
		}
		x++;
	}
	system("cls");
	gamestate = GAME_RUN;
}

bool validLocation(string location){
	string locX,locY;
	int xPos,yPos;
	if(location.length() == 3){
		locX = location[0];
		locY = location[2];
		
		xPos =  stoi(locX,0,10);
		yPos =  stoi(locY,0,10);

		if(validChoice(xPos,1,6) && validChoice(yPos,1,6)){
			return true;
		} else{
			return false;
		}
	}else
		return false;
}

bool validPlacement(int x, int y){
	int dirX[] = {1,-1,0,0};
	int dirY[] = {0,0,1,-1};

	if(galaxyMap[x][y] == " "){
		for(int i = 0; i < 4; i++){
			if(galaxyMap[x+dirX[i]][y+dirY[i]] == " "){

			} else {
				return true;
			}
		}
		return false;
	} else
		return false;
}

void placePlanet(int player, int x, int y){
	if(player == 0)
		galaxyMap[x][y] = "X";
	else if(player == 1)
		galaxyMap[x][y]  = "O";
	else if(player == 2)
		galaxyMap[x][y]  = "&";
	else if(player == 3)
		galaxyMap[x][y] = "$";
	else if(player == 4)
		galaxyMap[x][y]  = "K";
	else if(player == 5)
		galaxyMap[x][y]  = "%";

}

void printCurrentGalaxy(){
	cout << endl << "THE GALAXY VIEW" << endl << endl;
	string galaxy = "  1 2 3 4 5 6 \n";
	stringstream ss;
	for(int i = 0; i < 6; i++){
		ss << i+1;
		galaxy += ss.str();
		for(int j = 0; j < 6; j++){
			galaxy += "|" + galaxyMap[i][j];
		}
		galaxy += "|\n";
		ss.str("");
	}
	
	cout << galaxy;
}

void printPlayerDetails(){
	system("pause");
	system("cls");
	for(int i = 0; i < players.size(); i++){
		cout << "\n Player ID: " << players[i].getPlayerID() << ", Faction : " << players[i].getFaction() << 
				"\n Planets: " << returnPlayerPlanets(i) << endl;
	}
}

string returnPlayerPlanets(int i){
	string result = "";
	for(unsigned int j = 0; j < 12; j++){
		
		if(players[i].getPlanet(j) == -1){
			
		}else {
			result += planets[players[i].getPlanet(j)] + ", ";
		}
	}
	return result;
}

void printFactions(){
	for(int i = 0; i < 6; i++)
		cout << i+1 << ". " << faction[i] << "|	" << testFactionAvailablity(i) << "\n";
}

string testFactionAvailablity(int i){
	if(factionAvailablity[i])
		return "Available";
	else
		return "Not Available";
}

bool testPlanetAvailabilty(int i){
	if(planetAvailabilty[i])
		return true;
	else
		return false;
}

bool validChoice(int optionSelected, int minimumSelection, int maximumSelection){
	if(optionSelected >= minimumSelection && optionSelected <= maximumSelection)
		return true;
	else
		return false;
}