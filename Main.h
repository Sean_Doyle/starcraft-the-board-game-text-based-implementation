#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <ctime>

#include "Player.h"

using namespace std;

const string faction[6] = { "Queen of Blades	",
							"The Overmind		",
							"Jim Raynor		",
							"Arcturus Mengsk	",
							"Tassadar		",
							"Aldaris		"};

const string planets[12] = {"Helios", "Korhal", "Tarsonis", "Haven", "Ash'Arak", "Aiur", "Zerus", "Earth", "Char", "Mar Sara", "Dylar IV", "Nidhogg"};

string galaxyMap[6][6];

enum GAMESTATE{GAME_START, GAME_SETUP, GAME_RUN, GAME_END};
GAMESTATE gamestate;

int playerCount; //total number of players ingame
bool isRunning; //Controls main game loop

int main();
void startMenu();
void selectPlayerAmount();
void selectPlayerFactions();
void assignPlanets();
void printFactions();
void printPlayerDetails();
void galaxyInformation();
void printCurrentGalaxy();
void galaxyCreation();
string returnPlayerPlanets(int);
string testFactionAvailablity(int);
bool testPlanetAvailabilty(int);
bool validChoice(int, int, int);
bool validLocation(string);
bool validPlacement(int,int);
void placePlanet(int,int,int);
void quit();
