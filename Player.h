#include <string>
#include <iostream>

using namespace std;

class Player{
private:
	int id;
	string faction;
	int* planets;

public:
	Player(int, string);
	void setPlayerID(int);
	void setFaction(string);
	void setPlanets(int, int);

	int totalPlanetsOwned();
	int getPlayerID();
	string getFaction();
	int getPlanet(int);
};